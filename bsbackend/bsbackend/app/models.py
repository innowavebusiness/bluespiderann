import numpy as np
from tensorflow.keras.models import load_model


class Prediction:
    def __init__(self):
        self.MODELPATH = "bsbackend/app/ann/ann_classifier.h5"
        self.model = load_model(self.MODELPATH)

    def predict(self, array):
        model = self.model
        new_prediction_probability = model.predict(np.array([array]))
        print(new_prediction_probability)
        new_prediction_bool = (new_prediction_probability > 0.5)
        print(new_prediction_probability)

        result = {'prediction': new_prediction_bool,
                  'probability': new_prediction_probability}

        return result
