from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny
from rest_framework.status import (
        HTTP_200_OK,
        HTTP_400_BAD_REQUEST,
    )
from bsbackend.app.models import Prediction

Prediction = Prediction()

@api_view(['POST'])
@permission_classes((AllowAny,))
def predict(request):
    try:
        userdata = request.data['userdata']
        print(userdata)
    except KeyError:
        return Response({'userdata': ['no data']}, status=HTTP_400_BAD_REQUEST)

    result = Prediction.predict(userdata)
    print(result)

    return Response(result, status=HTTP_200_OK)

@api_view(['POST'])
@permission_classes((AllowAny,))
def test_view(request, format=None):
    return Response({'received data': request.data})
