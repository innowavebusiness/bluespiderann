import numpy as np

def predict(classifier, array):
    new_prediction_probability = classifier.predict(np.array([array]))
    # print(new_prediction_probability)
    new_prediction_bool = (new_prediction_probability > 0.5)
    # print(new_prediction_probability)
    return [new_prediction_bool, new_prediction_probability]
