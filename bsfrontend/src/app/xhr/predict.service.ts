import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Person } from './person.interface';
import { Country, Profession, Gender } from '../model/lookup-table';
import { HttpClient } from '@angular/common/http';
import { Prediction } from './prediction.interface';


@Injectable({
  providedIn: 'root'
})
export class PredictService {

  constructor(private http: HttpClient) { }

  post(body: {userdata: Array<number>}): Observable<Prediction>{
    return this.http.post<Prediction>("http://localhost:8000/predict/", body);
  }

  postForm(person: Person): Observable<{prediction: boolean, probability: number}> {
    const userdata = new Array(11);
    userdata[0] = 0;
    userdata[1] = +Country[person.country];
    userdata[2] = +person.profession;
    userdata[3] = +Gender[person.gender];
    userdata[4] = +person.age
    userdata[5] = +person.pplAtHome
    userdata[6] = +person.cronicIllness
    userdata[7] = +person.healthyPhysical
    userdata[8] = +person.massTransit
    userdata[9] = +person.suspect15d
    userdata[10] = +person.suspect30d
    
    return this.post({userdata}).pipe(map( (resp: Prediction) => {
      const prediction = resp.prediction[0];
      const probability = resp.probability[0];
      return {prediction, probability};
    }));
  }

}
