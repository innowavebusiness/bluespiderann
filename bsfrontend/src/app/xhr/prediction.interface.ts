export interface Prediction {
    prediction: Array<boolean>
    probability: Array<number>
}