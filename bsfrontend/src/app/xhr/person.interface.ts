export interface Person {
    country : string,
    profession : string,
    gender : string,
    age : number,
    pplAtHome : number,
    cronicIllness : boolean,
    healthyPhysical : boolean,
    massTransit : boolean,
    suspect15d : boolean,
    suspect30d : boolean,
}
