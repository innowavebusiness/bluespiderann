import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PersonPredictFormComponent } from './ui/person-predict-form/person-predict-form.component';


const routes: Routes = [
  {path: "", component: PersonPredictFormComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
