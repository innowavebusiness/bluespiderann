import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { PredictService } from 'src/app/xhr/predict.service';
import { Person } from 'src/app/xhr/person.interface';
import { HttpErrorResponse } from '@angular/common/http';
import { prefill } from './prefill.examples';
import { Country, Profession, Gender } from 'src/app/model/lookup-table';

@Component({
  selector: 'app-person-predict-form',
  templateUrl: './person-predict-form.component.html',
  styleUrls: ['./person-predict-form.component.scss']
})
export class PersonPredictFormComponent implements OnInit {

  constructor(private predictService: PredictService) { }

  result: {prediction: boolean, probability: number};

  debug: {code: number, error: string};

  translateMap = new Map([
    ["country", "Country"], 
    ["profession", "Profession"],
    ["gender", "Gender"],
    ["age", "Age"],
    ["pplAtHome", "Number of persons living under the same roof"],
    ["cronicIllness", "Has a chronic decease"],
    ["healthyPhysical", "Practices a regular physical activity"],
    ["massTransit", "Uses Public Transportation"],
    ["suspect15d", "Had Symptoms during the last 15 days"],
    ["suspect30d", "Had Symptoms during the last 30 days"],
    ["fr","France"],
    ["be","Belgium"],
    ["nl","The Netherlands"],
    ["ger","Germany"],
    ["es","Spain"]
  ]);


  form = new FormGroup({
    country :         new FormControl('fr'),
    profession :      new FormControl(50),
    gender :          new FormControl('male'),
    age :             new FormControl(30),
    pplAtHome :       new FormControl(5),
    cronicIllness :   new FormControl(false),
    healthyPhysical : new FormControl(false),
    massTransit :     new FormControl(true),
    suspect15d :      new FormControl(false),
    suspect30d :      new FormControl(false),
  });

  translate(field: string): string {
    return this.translateMap.get(field);
  }
  // Number: 0   // dummy
  // Id: 1789563 //
  // Name: John  //
  // Country: France
  // Profession Code: 49
  // Gender: Male
  // Age: 22
  // Number of persons living under the same roof: 3
  // Has a chronic decease: 1
  // Practices a regular physical activity: 0
  // Uses Public Transportation: 1
  // Had Symptoms during the last 15 days: 1
  // Had symptoms during the last 30 days: 0"""

  ngOnInit(): void {
    console.log(Object.keys(Country)
    .map(entry => Country[entry])
    .filter(isNaN))
  }

  Profession(nr: number) {
    return Profession[nr];
  }

  times(nr: number) {
    return Array(nr).fill(0).map((x,i)=>i)
  }


  getAllCountries = () => Object.keys(Country).map(entry => Country[entry]).filter(isNaN);
  getAllProfessions = () => Object.keys(Profession).map(entry => Profession[entry]).filter(isNaN);
  getAllGender = () => Object.keys(Gender).map(entry => Gender[entry]).filter(isNaN);

 

  getFormFields() {
    const fields = [];
    const controls = this.form.controls;
    for(let name in controls) {
      fields.push({name, type: typeof controls[name].value});
    }
    return fields;
  }

  prefil(id: number) {
    this.form.setValue(prefill[id])
  }

  

  submit() {
    this.predictService.postForm(this.form.value as Person).subscribe(resp => {
      console.log(resp.prediction, resp.probability);
      this.result = resp;
      this.debug = undefined;
    },
    (error:HttpErrorResponse) => {
      this.result = undefined;
      this.debug = {code:error.error, error:error.statusText}
    });
  }
}
