import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PersonPredictFormComponent } from './person-predict-form.component';

describe('PersonPredictFormComponent', () => {
  let component: PersonPredictFormComponent;
  let fixture: ComponentFixture<PersonPredictFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PersonPredictFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonPredictFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
