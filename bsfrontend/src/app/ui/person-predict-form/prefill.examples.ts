import { Profession } from 'src/app/model/lookup-table';

export const prefill: Array<any> = [
    {
        country :         'fr',
        profession :      Profession.Photographer,
        gender :          'male',
        age :             3,
        pplAtHome :       1,
        cronicIllness :   false,
        healthyPhysical : false,
        massTransit :     false,
        suspect15d :      false,
        suspect30d :      false,
      },
      {
        country :         'ger',
        profession :      Profession.Carpenter,
        gender :          'male',
        age :             39,
        pplAtHome :       7,
        cronicIllness :   false,
        healthyPhysical : false,
        massTransit :     true,
        suspect15d :      false,
        suspect30d :      false,
      },
      {
        country :         'es',
        profession :      Profession.Receptionist,
        gender :          'male',
        age :             36,
        pplAtHome :       9,
        cronicIllness :   true,
        healthyPhysical : true,
        massTransit :     true,
        suspect15d :      false,
        suspect30d :      true,
      }
];

export const prefilArray = [
    [0, 0, 49, 1, 3, 1, 0, 0, 0, 0, 0],
    [0, 0, 49, 1, 3, 1, 0, 0, 0, 0, 0],
    [0, 0, 49, 1, 3, 1, 0, 0, 0, 0, 0]
  ]