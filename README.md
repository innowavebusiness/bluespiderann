# README #

Since the beginning of the Covid-19 pandemic, almost half of the world population (4.5 billion people) is called upon to confine itself.
The sanitary crisis has led to an economical and social crisis. Looking forward to a future progressive decontainement, it would be useful to identify the people who can safely go back to their normal activities, and the people who still are at risk of acting as a propagation vector for the Sars-cov2 virus.

This repository aims to provide code prototype to train Deep Learning models to help identify people who are most likely to act as a propagation vector.
The models are trained based on a CSV file embedding dummy qualitative and quantitative hypothetical criteria. 

### Disclaimer ###

The data provided in this repository are **dummy** and **not accurate** and has been provided for DEMO purpose only. For the models to be reliable, the data in the profiles.csv file would need to be edited or replaced with an accurate dataset.

### Use case ###

#### Hypothesis ####
* There is no possibility of testing the infection of each individual in a population.
* A propagation vector is an individual (a person) who transmits the virus to other people.
* The fact of an individual being a propagation vector or not, can be explained by other variables (Medical condition, use of public transportation services... etc.).

#### Problematic ####
Considering a progressive decontainement of a population, we assume the individuals who are the less likely to act as a propagation vector for the Covid-19, will be the first to be cleared for decontainement.
We therefore can formulate the following problematic:
"How can we determine who are the individuals who has the best chances of reintegration in the active life, without presenting a threat to the population by propagating the Sars-cov2 virus?"

### Repository content ###
#### ANN ####
* ann.py (The Artificial Neural Network code)
* predict.py (The piece of code used to make predictions)
* data/profiles.csv (The data used to train the ANN)
* models/ann_classifier.h5 (The h5 file embedding the trained model)

#### bsbackend (Django server exposing a REST API) ####
* Django server (Please check official Django [documentation](https://docs.djangoproject.com/en/3.0/) for how to deploy and operate an Django server)
* 'bsbackend/app' is the directory holding the app code (views, models and functions)
* The trained ann h5 file is stored by default in 'bsbackend/app/ann'. This file is the output of the trained model using Keras.

#### Version ####
0.02 Alpha (April 2020)

### Installation instructions ###

#### Requirements: ####
#### ANN ####
* Python 3.6
* [Keras](https://keras.io/) neural networks API   
* [scikit-learn](https://scikit-learn.org/)
* [matplotlib](https://matplotlib.org/)
* [numpy](https://numpy.org/)
* [pandas](https://pandas.pydata.org/)
* We used Tensorflow as backend for Keras: 'pip install --upgrade tensorflow' or 'pip install --upgrade tensorflow-gpu'. See detailed installation instructions for Tensorflow on https://www.tensorflow.org/install/

#### bsbackend ####
Assuming you have Django (we use v3.0.3) installed, the additional following libraries are required:
* numpy
* tensorflow
* keras
* djangorestframework
* django-cors-headers 

#### Quick start ####
##### ANN #####
* Run the command 'pip install -r requirements.txt' in a terminal
* Train the model 'python train.py'
* Edit the predicted profile in predict.py file
* Run 'python predict.py'

##### Backend #####
* `'cd bsbackend'`
* Install [Django](https://docs.djangoproject.com/en/3.0/intro/install/) and the requirements: 'pip install -r requirements.txt' 
* Start Django `'python manage.py runserver'`
* The REST service is exposed on http://127.0.0.1:8000/
    The service is allowing only POST and OPTIONS methods.
* The 'predict' view expects a POST request submitting a list of parameters such as  
`curl --location --request POST 'http://127.0.0.1:8000/predict/' \
--header 'Content-Type: application/json;charset=UTF-8' \
--data-raw '{
	"userdata" : [0, 0, 49, 1, 22, 3, 1, 0, 1, 1, 1]
}' `
* In case the request is rejected, check that the content type is 'application/json', and that the encoding is UTF-8. This can be set in the headers: 
`Content-Type : application/json;charset=UTF-8`

##### Frontend #####
* having nodejs installed from nodejs.org
* `'cd bsfrontend'`
* npm install
* npm install -g @angular/cli
* ng serve


### Contributors ###
* Developers and maintainers: O.Achhab and P.Pletinckx

### Licence  ###

Copyright 2020 [MIT](https://opensource.org/licenses/MIT) License 

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

