# Importing libraries
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.compose import ColumnTransformer
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Dropout


def build_classifier():
    classifier = Sequential()
    classifier.add(Dense(units=6, kernel_initializer='uniform', activation='relu', input_dim=11))
    classifier.add(Dropout(0.05))
    classifier.add(Dense(units=6, kernel_initializer='uniform', activation='relu'))
    classifier.add(Dropout(0.05))
    classifier.add(Dense(units=1, kernel_initializer='uniform', activation='sigmoid'))
    classifier.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    return classifier


def predict(classifier, array):
    new_prediction_probability = classifier.predict(np.array([array]))
    # print(new_prediction_probability)
    new_prediction_bool = (new_prediction_probability > 0.5)
    # print(new_prediction_probability)
    return [new_prediction_bool, new_prediction_probability]


def import_data_csv(path_to_csv):
    # Importing data from csv file
    dataset = pd.read_csv(filepath_or_buffer=path_to_csv, delimiter=';')
    X = dataset.iloc[:, 3:13].values
    y = dataset.iloc[:, 13].values
    # Encoding categorical data
    tmp = ColumnTransformer([("Country", OneHotEncoder(), [1])], remainder='passthrough')
    X = tmp.fit_transform(X)
    labelencoder_x_2 = LabelEncoder()
    X[:, 4] = labelencoder_x_2.fit_transform(X[:, 4])
    X = X[:, 1:]

    return [X, y]


def show_plot(model):
    plt.plot(model.history['accuracy'], label='TrainingACC')
    plt.legend()
    plt.show()

    plt.plot(model.history['loss'], label='TrainingLoss')
    plt.legend()
    plt.show()
