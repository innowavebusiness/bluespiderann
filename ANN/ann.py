# Importing libraries
from ANN.functions import build_classifier,import_data_csv, show_plot
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

# Importing the dataset

dataset = import_data_csv(path_to_csv='ANN/data/profiles.csv')
X = dataset[0]
y = dataset[1]

# Splitting the dataset into the Training set and Test set
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Feature Scaling
sc = StandardScaler()
X_train = sc.fit_transform(X_train)
X_test = sc.transform(X_test)

# Initialising the ANN

classifier = build_classifier()

# Fitting the ANN to the Training set
model = classifier.fit(X_train, y_train, batch_size=256, epochs=100)

show_plot(model)

# Saving the ANN to h5 model
classifier.save('ANN/models/ann_classifier.h5')
