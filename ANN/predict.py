# Importing libraries
from keras.models import load_model
from ANN.functions import predict

if __name__ == "__main__":
    # loading model
    classifier = load_model("ANN/models/ann_classifier.h5")

    # Predicting a new observation

    """Predict if the individual with the following data will act as a propagation vector for the covid-19:
    Number: 0
    Id: 1789563
    Name: John Doe
    Country: France
    Profession Code: 49
    Gender: Male
    Age: 22
    Number of persons living under the same roof: 3
    Has a chronic decease: 1
    Practices a regular physical activity: 0
    Uses Public Transportation: 1
    Had Symptoms during the last 15 days: 1
    Had symptoms during the last 30 days: 1"""

    values = [0, 0, 49, 1, 22, 3, 1, 0, 1, 1, 1]

    new_prediction = predict(classifier, values)

    print("The individual with the following data will act as a propagation vector for the covid-19 : " + str(new_prediction[0][0][0]))
    print("With a probability of : " + str(new_prediction[1][0][0]))
